var jsApp = {
    init: function ()
    {
        $('#goButton').on('click', function (e) {
            e.preventDefault();

            $('.error').remove();

            if($('#urlField').val() == '' | !jsApp.validateURL($('#urlField').val()))
            {
                $('#goButton').after('<p class="error">That doesn\'t look right...<br /> It should be something like http://open.spotify.com/user/something/playlist/73128987389</p>');
                return false;
            }

            window.onpopstate = function(e){
                if(e.state){
                    jsApp.reset();
                }
            };

            $('#backButton').on('click', function (e) {
                e.preventDefault();
                jsApp.reset();
            });

            $('#goButton i').hide();
            $('#goButton .spinner').show();
            $('label[for="urlField"]').slideUp();
            $('#urlField').prop('disabled', true);
            $('#loadingMessage').html('Loading...');
            $('#loadingMessage').slideDown();

            $.ajax({
                data: {
                    uri: $('#urlField').val(),
                    width: $('form').width()
                },
                cache: false,
                type: 'POST',
                dataType: 'json',
                timeout: 100000,
                url:'/ajax/get',
                success: function (response)
                {
                    $('#goButton i').show();
                    $('#goButton .spinner').hide();
                    $('label[for="urlField"]').slideDown();
                    $('#urlField').prop('disabled', false);
                    $('#loadingMessage').hide();

                    $('form').slideUp();
                    $('#result').show();
                    $('#iframeContainer').html(response.data.iframe);
                    $('#playlistTitle').html(response.data.title);

                    window.history.pushState({'pageTitle':response.data.title},"", '?uri=' + response.data.uri);
                },
                error: function ()
                {
                    $('#goButton').after('<p class="error">Something went wrong. It\'s probably your fault, but don\'t feel too bad about it. Just try it again.</p>');

                    $('#goButton i').show();
                    $('#goButton .spinner').hide();
                    $('label[for="urlField"]').slideDown();
                    $('#urlField').prop('disabled', false);
                    $('#loadingMessage').hide();
                }
            });

            setTimeout(function () {
                $('#loadingMessage').html('Give it a second...');
            }, 10000);

            setTimeout(function () {
                $('#loadingMessage').html('Hold on...');
            }, 20000);

            setTimeout(function () {
                $('#loadingMessage').html('I swear this goes pretty swiftly normally.');
            }, 30000);

            setTimeout(function () {
                $('#loadingMessage').html('Right, this is just getting ridiculous now.');
            }, 40000);

            setTimeout(function () {
                $('#loadingMessage').html('Still? Still loading? Well this won\'t do at all now, will it. Email us at <a href="mailto:bugs@altocinco.com">bugs@altocinco.com</a> with the link of the playlist and we\'ll look into it.');
            }, 100000);
        });

        if(typeof($('#urlField').data('url')) != 'undefined')
        {
            $('#urlField').val($('#urlField').data('url'));
            $('#goButton').click();
        }
    },

    reset: function ()
    {
        $('#goButton i').show();
        $('#goButton .spinner').hide();
        $('form').slideDown();
        $('#result').hide();
        $('#iframeContainer').html('');
        $('label[for="urlField"]').show();
        $('#urlField').prop('disabled', false);
        $('#loadingMessage').html('Loading...');
        $('#loadingMessage').hide();
        $('#urlField').val('');

        window.history.pushState({'pageTitle':'Too Cheap For Spotify'},"", '');
    },

    validateURL: function (url)
    {
        var possibleURLS = ['open.spotify.com', 'play.spotify.com', 'spotify:'];
        var ok = false;
        for(var i in possibleURLS)
        {
            if(url.lastIndexOf(possibleURLS[i]) != -1) ok = true;
        }

        return ok;
    }
}
$(jsApp.init);