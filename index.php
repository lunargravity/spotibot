<?php

error_reporting(E_ALL | E_STRICT);
ini_set('display_errors', 'On');

require_once __DIR__ . '/vendor/autoload.php';
use Silex\Provider\FormServiceProvider;

$app = new Silex\Application();

require_once __DIR__ . '/src/core/config.php';
require_once __DIR__ . '/src/core/controller.php';
require_once __DIR__ . '/src/core/ajax.php';

$app['debug'] = true;

$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => PATH_SRC . '/views',
));
$app->register(new FormServiceProvider());

$app->match('/{action}', function ($action) use($app)
{

    if(file_exists(PATH_SRC . '/controllers/' . $action . '.php'))
    {
        require_once PATH_SRC . '/controllers/' . $action . '.php';
        require_once PATH_SRC . '/model/model.php';
        $className = ucfirst($action) . 'Controller';
        $controller = new $className($action);
        $controller->app = $app;
        $controller->init();

        $output = $app['twig']->render('header.html', array('pageTitle' => $controller->pageTitle));
        $output .= $controller->render();
        $output .= $app['twig']->render('footer.html');

        return $output;
    }

    return 'What you\'re looking for doesn\'t exist';
})->value('action', 'index');


$app->match('/ajax/{action}', function ($action) use($app)
{
    if(file_exists(PATH_SRC . '/ajax/' . $action . '.php'))
    {
        require_once PATH_SRC . '/ajax/' . $action . '.php';
        require_once PATH_SRC . '/model/model.php';
        $className = 'AjaxController' . ucfirst($action);
        $controller = new $className();
        $controller->app = $app;
        return $controller->execute();
    }

    return 'What you\'re looking for doesn\'t exist';
});

$app->run();
?>