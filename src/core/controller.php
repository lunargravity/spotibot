<?php

class BaseController
{
    public $template;
    public $templateVars;
    public $app;
    protected $name;
    public $pageTitle = '';

    public function __construct($name)
    {
        $this->name = $name;
    }

    public function init()
    {
    }

    protected function assign($name, $value)
    {
        $this->templateVars[$name] = $value;
    }

    public function render()
    {
        return $this->app['twig']->render($this->name . '.html', $this->templateVars);
    }
}