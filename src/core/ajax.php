<?php

class BaseAjaxController
{
    public $app;

    public function execute()
    {

    }

    public function output($code, $data = array(), $message = '')
    {
        $return = array();
        $return['code'] = $code;
        $return['data'] = $data;
        $return['message'] = $message;

//        header('Content-Type: application/json');
        return json_encode($return);
    }
}