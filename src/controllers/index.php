<?php

class IndexController extends BaseController
{
    public $pageTitle = 'Spotibot';

    function init()
    {
        if(isset($_GET['uri']))
        {
            $this->assign('uri', $_GET['uri']);
        } elseif(isset($_POST['uri']))
        {
            $this->assign('uri', $_POST['uri']);
        } else $this->assign('uri', false);
    }


}