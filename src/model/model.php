<?php

class AppModel {
    const YOUTUBE_API_KEY = 'AIzaSyC1b6GPEcuJhlgHrym_EozNVTHg6t_ne5M';

    public static function getHTTPContent($URL)
    {
        // set options
        $options[CURLOPT_URL] = (string) $URL;
        $options[CURLOPT_USERAGENT] = 'TCFS';
        if(ini_get('open_basedir') == '' && ini_get('safe_mode' == 'Off')) $options[CURLOPT_FOLLOWLOCATION] = true;
        $options[CURLOPT_RETURNTRANSFER] = true;
        $options[CURLOPT_TIMEOUT] = 10;

        // init
        $curl = curl_init();

        // set options
        curl_setopt_array($curl, $options);

        // execute
        $response = curl_exec($curl);

        // fetch errors
        $errorNumber = curl_errno($curl);
        $errorMessage = curl_error($curl);

        // close
        curl_close($curl);

        // validate
        if($errorNumber != '') throw new Exception($errorMessage);

        // return the content
        return (string) $response;
    }

    public static function process($uri)
    {
        $embedContent = self::getHTTPContent('https://embed.spotify.com/?uri=' . $uri);
        $embedDOM = new DOMDocument();
        @$embedDOM->loadHTML($embedContent);

        $tracks = array();
        $ids = array();

        $title = $embedDOM->getElementsByTagName('title')->item(0)->textContent;

        foreach ($embedDOM->getElementsByTagName('ul') as $node) {
            if(strpos($node->getAttribute('class'), 'track-info') !== false)
            {
                $newTrack = array();
                $listItems = $node->getElementsByTagName('li');
                $newTrack['title'] = substr($listItems->item(1)->textContent, 3);
                $newTrack['artist'] = $listItems->item(2)->textContent;

                $youtubeURL = 'https://www.googleapis.com/youtube/v3/search?';
                $youtubeParams = array('part' => 'snippet', 'q' =>  $newTrack['artist'] . ' ' . $newTrack['title'], 'key' => self::YOUTUBE_API_KEY, 'maxResults' => 1, 'type' => 'video');
                $ytContent = self::getHTTPContent($youtubeURL . http_build_query($youtubeParams));
                $ytVideos = json_decode($ytContent);
                if(!empty($ytVideos->items))
                {
                    foreach($ytVideos->items as $ytVideo)
                    {
                        $ids[] = $ytVideo->id->videoId;
                    }
                }
            }
        }

        return array($title, $ids);
    }
}