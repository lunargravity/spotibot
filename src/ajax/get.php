<?php

class AjaxControllerGet extends BaseAjaxController
{
    public function execute()
    {
        if(isset($_REQUEST['uri']))
        {
            $uri = $_REQUEST['uri'];
        }
        else $this->output(400, array(), 'No uri set');

        $uri = str_replace('http://', '', $uri);
        $uri = str_replace('https://', '', $uri);

        if(strpos($uri, 'open.spotify.com/') !== false)
        {
            $uri = substr($uri, strlen('open.spotify.com/'));
            $uri = 'spotify:' . str_replace('/', ':', $uri);
        }
        elseif(strpos($uri, 'play.spotify.com/') !== false)
        {
            $uri = substr($uri, strlen('open.spotify.com/'));
            $uri = 'spotify:' . str_replace('/', ':', $uri);
        }

        list($title, $trackIds) = AppModel::process($uri);

        // remove by from title
        $title = substr($title, 0, strrpos($title, ' by '));

        $firstTrack = array_shift($trackIds);

        $width = $_REQUEST['width'];
        $height = ($width / 4) * 3;

        $iframeCode = '<iframe width="' . $width . '" height="' . $height . '" src="//www.youtube.com/embed/' . $firstTrack . '?playlist=' . implode(',', $trackIds) . '" frameborder="0" allowfullscreen></iframe>';

        return $this->output(200, array('iframe' => $iframeCode, 'title' => $title, 'uri' => $uri));
    }
}